package org.seipiales;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;



/**
* EjercicioCasa.java 
 * Autoras: Guaminga A., G�mez V., Ipiales S.
 * Este programa contiene el escenario 1:
 * 1.	Casa
 * 2.	Nube que se mueva con mouse
 * 3.	Un Carro que se mueva con fechas de navegaci�n
 * 2020-08-06
 */

public class EjercicioCasa extends JFrame  implements KeyListener{
     //Variables para instanciar funciones de opengl
    static GL gl;
    float trasladaX1=0;
    static GLU glu;
    public EjercicioCasa() {
        setSize(840, 680);
        setLocation(10, 40);
        setTitle("Escenario Casa-Carro");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
       
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);     
        
    }

    public static void main(String args[]) {
        EjercicioCasa frame = new EjercicioCasa();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
   
    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {
            //Inicializamos las variables
            //INICIALIZA GLU
            glu = new GLU();
            //Inicializa Gl
            gl = arg0.getGL();
            gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glClearColor(0.0384f, 0.3107f, 0.48f, 0.0f);            
            

            gl.glOrtho(-350, 350, 350, -350, -200, 200);
            gl.glLoadIdentity();
            Casa casa= new Casa();
            Carro c= new Carro();
            Nube n= new Nube();

            //Casa
            gl.glPushMatrix();
            gl.glScalef(0.07f, 0.1f, 0);
            casa.DibujarCasa(gl);
            gl.glPopMatrix();
            
            //Carro               
            gl.glPushMatrix();
            gl.glTranslatef(0, -0.5f, 0);
            gl.glScalef(0.07f, 0.1f, 0.1f);
            //Aplicar movimiento al carro
            gl.glTranslatef(trasladaX, trasladaY, 0f);     
            c.DibujarCarro(gl); 
            gl.glPopMatrix();
            
            //NUBE
            gl.glPushMatrix();
            gl.glTranslatef(-0.2f, -0.6f, 0);
            gl.glScalef(0.07f, 0.1f, 0.0f); 
            gl.glTranslatef(trasladaX1, 0, 0.0f)  ;
            n.DibujarNube(gl);
            gl.glPopMatrix();
            if(trasladaX1<=9.11f){
               trasladaX1+=0.01; 
            }else{
                trasladaX1=-10.3f;
            }
            
            gl.glFlush();
            
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }
//Variables para las transformaciones
    static float trasladaX=0;
    static float trasladaY=0;
   
    
    //Metodo para el teclado
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_RIGHT){
            if(trasladaX<=9.11f){
            trasladaX+=.1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
            }else{
                trasladaX=-10.3f;
            }
         }
        if(e.getKeyCode()==KeyEvent.VK_LEFT){
            if (trasladaX>=-10.3f) {
               trasladaX-=.1f; 
               System.out.println("Valor en la traslacion de X: " + trasladaX);
            }else{
                trasladaX=9.11f;
            }
        }
        if(e.getKeyCode()==KeyEvent.VK_UP){
            if (trasladaY<0.6) {
                 trasladaY+=.1f;
                 System.out.println("Valor en la traslacion de Y: " + trasladaY);
            }else{
                trasladaY=0.6f;
            }
           
        }
        if(e.getKeyCode()==KeyEvent.VK_DOWN){
            if(trasladaY>=-1.2f){
                trasladaY-=.1f;
                 System.out.println("Valor en la traslacion de Y: " + trasladaY);
            }else{
                trasladaY=-1.2f;
            }
            
        }
       
      
        if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            trasladaX=0;
            trasladaY=0;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
}

