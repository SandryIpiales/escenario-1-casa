/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GL;
import static org.seipiales.EjercicioCasa.gl;
import org.seipiales.Figuras;
import org.seipiales.Figuras;

/**
 *
 * @author lenovo
 */
public class Carro  {
    
    static float trasladaX=0;
    static float trasladaY=0;
    static float trasladaZ=0;
    static float angulo=0;
    
    
    public Carro(){
        
    }
    public void DibujarCarro(GL gl){
        
        Figuras figura= new Figuras();
        gl.glPushMatrix();
        //Dibujar Mitad Decagono(Parte de Arriba)
        
        figura.DecagonoMitad(gl,  0.4f, 0.6102f, 0.64f,0.f, 0.5775f, 0.75f, 3.4f, 3.0f, 4.0f, 0.8f, 1.0f, 0f,-1.0f);
//        figura.DibujarTrapecio(gl, 0.44f, 0.4374f, 0.4312f, 0.7f, 0.5887f, 0.329, 3.4f, -3.4f, -3f, 3f, 0.8f, 1.0f);
        figura.DibujarCuadrado2Color(gl,  0.66f, 0.4422f,0.4712f,0.49f, 0.1481f,0.0441f, -4.0f, 4.0f, -0.7f, -3.0f);//Base Auto
       
        //Puertas
        gl.glPushMatrix();
        gl.glTranslatef(-5.0f,0.0f, 0.0f);
        figura.DibujarCuadrado2Color(gl, 1.0f, 2.0f, 0f, -2.5f, -7.5f, 0f, 1.0f, 3.75f, -0.8f, -2.9f);
        gl.glTranslatef(2.85f,-0.0f, 0.0f);
        figura.DibujarCuadrado2Color(gl, 1.0f, 2.0f, 0f, -2.5f, -7.5f, 0f, 1.0f, 3.75f, -0.8f, -2.9f);
        gl.glTranslatef(2.85f,-0.0f, 0.0f);
        figura.DibujarCuadrado2Color(gl, 1.0f, 2.0f, 0f, -2.5f, -7.5f, 0f, 1.0f, 3.2f, -0.8f, -2.9f);        
        gl.glPopMatrix();
        //FRANJA
        figura.DibujarCuadrado2Color(gl, 0.93f, 0.5952f, 0.6398f,0.55f,0.3107f, 0.055f,   4f, -4f,-0.5f, -0.8f);
        //Ventanas
        gl.glPushMatrix(); 
        gl.glTranslatef(-4.6f, 0f, 0f);
        figura.PoligonoVentanas(gl, 0, 0, 0, 0, 0, 0, 1.5f, 1.0f, 0.8f, 3.5f, 3.3f, 2.7f, 0.8f, 0.5f, 0f, -0.5f);
        figura.PoligonoVentanas(gl, 0.6237f, 0.874f, 0.99f, 0.7905f,0.8858f, 0.93f, 1.5f, 1.0f, 0.9f, 3.4f, 3.3f, 2.7f, 0.7f, 0.4f, 0f, -0.5f);
        gl.glTranslatef(2.79f, 0f, 0f);
        figura.PoligonoVentanas(gl, 0, 0, 0, 0, 0, 0, 1.5f, 1.0f, 0.8f, 3.5f, 3.3f, 2.7f, 0.8f, 0.5f, 0f, -0.5f);
        figura.PoligonoVentanas(gl, 0.6237f, 0.874f, 0.99f, 0.7905f,0.8858f, 0.93f, 1.5f, 1.0f, 0.9f, 3.4f, 3.3f, 2.7f, 0.7f, 0.4f, 0f, -0.5f);
        gl.glTranslatef(2.78f, 0f, 0f);
        figura.PoligonoVentanas(gl, 0, 0, 0, 0, 0, 0, 1.5f, 1.0f, 0.8f, 2.98f, 2.7f, 2.1f, 0.8f, 0.5f, 0f, -0.5f);//Marco
        figura.PoligonoVentanas(gl, 0.6237f, 0.874f, 0.99f, 0.7905f,0.8858f, 0.93f, 1.5f, 1.0f, 0.9f, 2.9f, 2.7f, 2.0f, 0.7f, 0.4f, 0f, -0.5f);//Ventanas
        gl.glPopMatrix();
        gl.glPushMatrix();
        gl.glTranslatef(4.2f, -3.5f, .0f);
        gl.glScalef(0.4f, 1.2f, 0f); 
        gl.glRotatef(180, 1.0f, 1.0f, 0.0f);//Rotamos el mismo poligono 
       
        figura.PoligonoVentanas(gl, .0f, 0.0f, 0.0f, 0.f, 0.0f, 0f, 1.5f, 1.0f, 0.9f, 2.8f, 2.7f, 2.0f, 0.7f, 0.4f, 0f, -0.5f);
        
        gl.glPopMatrix();
        //LLANTAS      
         
        gl.glPushMatrix();
//        angulo=0;
       

        
        figura.Circulo(gl, -2.2f, -3.2f,.6f);
        figura.Circulo(gl, 2.2f, -3.2f,.6f);
        gl.glColor3f(0.52f, 0.507f, 0.4888f);
        figura.Circulo(gl, -2.2f, -3.2f,.3f);
        figura.Circulo(gl, 2.2f, -3.2f,.3f);  
        
        gl.glPopMatrix(); 
        
        angulo+= .10f;
       
        
    }   
}
