
package org.seipiales;

import javax.media.opengl.GL;


public class Casa {

    public Casa() {
    }
    
    public void DibujarCasa(GL gl){
        Figuras figura= new Figuras();
        //DIbujar Arboles
        figura.DibujarCuadrado2Color(gl, 0.32f, 0.2268f, 0.0256f, 0.32f, 0.2268f, 0.0256f, -15f,15f, -4.5f, -11f);
        figura.DibujarCuadrado2Color(gl, 0.84f, 0.8145f, 0.7812f, 0.84f, 0.8145f, 0.7812f, -15f,15f, -7.5f, -11f);
        figura.DibujarCuadrado2Color(gl, 0.4f, 0.2822f, 0.028f, 0.4f, 0.2822f, 0.028f, 8f, 9f,-3f , -6f);
        figura.DibujarCuadrado2Color(gl, 0.4f, 0.2822f, 0.028f, 0.4f, 0.2822f, 0.028f, 12f, 13f,-3f , -6f);
        figura.DibujarCuadrado2Color(gl, 0.4f, 0.2822f, 0.028f, 0.4f, 0.2822f, 0.028f, -8f, -9f,-3f , -6f);
        figura.DibujarTriangulo(gl, 0.045f, 0.5f, 0.3332f, 0.045f, 0.5f, 0.3332f, 8.5f, 9.3f,7.7f , -0, -4);
        figura.DibujarTriangulo(gl, 0.045f, 0.5f, 0.3332f, 0.045f, 0.5f, 0.3332f, -8.5f, -9.3f,-7.7f , -0, -4);
        figura.DibujarTriangulo(gl, 0.045f, 0.5f, 0.3332f, 0.045f, 0.5f, 0.3332f, 12.5f, 13.3f,11.7f , -0, -4);
        gl.glPushMatrix();
        gl.glScalef(0.9f, 0.9f, 0f);
        
        figura.DibujarCuadrado2Color(gl, 0.5f, -8.0f, 0.0f, 0.5f, -8.0f, 0f, -5f, 3.5f, -1f, -5.2f);//Dibujar el cuadrado parte de atras
       
        figura.DibujarTrapecio(gl,0f, 0f, .0f, 7.5f, 0.5f, 1.0f, -2.9f, 2.8f, -3.9f, 3.8f,0.5f, -1.2f); // Dibujar trapecio (techo atras)
        
        figura.DibujarCuadrado2Color(gl,7.0f, 3.0f, 0.0f, 0.87f, 0.5168f, 0.1392f, 0.1f, 3.5f, -1.5f, -1.8f);//Franja rectangular 
        
        figura.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, 0.5f, 3.3f, -2.0f, -4.8f);//Marco Ventanas
        figura.DibujarCuadrado2Color(gl, 7.0f, 3.0f, 0.0f, 5.2f, -1.0f, 6.0f, 0.7f, 3.2f, -2.1f, -4.7f);
        gl.glPopMatrix();
        // Movemos el dibujo
        
        gl.glTranslatef(-3.7f, .7f, -8.0f);// Movemos el dibujo
        // Dibujamos techo triangular
        gl.glScalef(0.7f, 0.7f, 0f);
        figura.DibujarTriangulo(gl,0.f, 0f, .0f,7.5f, 0.5f, 1.0f, 1.0f, -5.5f, 7.5f, 3.0f, -1.09f);      
        //Base de la casa parte del frente
        figura.DibujarPentagono(gl, 0.5f, -8.0f, 0.0f,0.5f, -8.0f, 0.0f, 1.0f, 6.0f, -4.0f, 2.0f, -1.09f, -9.09f);
        //MARCOS
        figura.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, 1.5f, 3.0f, 0, -2.5f);       //Marco Ventanas
        figura.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, -1.5f, 0f, 0, -2.5f);        
        figura.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, 2.5f, -0.7f, -4.0f, -9.1f);  //Marco Puerta 
        figura.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, 3.0f, 5.5f, -4.0f, -8.5f);   //Marco ventana parte de abajo 
        figura.DibujarCuadrado2Color(gl, 0, 0, 0, 0, 0, 0, -1.0f, -3.5f, -4.0f, -8.5f);
        //Franja arriba de la puerta
        figura.DibujarCuadrado2Color(gl, 7.0f, 3.0f, 0.0f, 0.87f, 0.5168f, 0.1392f, -4.0f, 6.0f, -3.5f, -2.9f);
        //VENTANAS
        figura.DibujarCuadrado2Color(gl, 0.5f, 3.5f, 1.0f, 5.0f, 3.5f, 0.0f, 1.7f, 2.8f, -0.2f, -2.3f);
        figura.DibujarCuadrado2Color(gl, 0.5f, 3.5f, 1.0f, 5.0f, 3.5f, 0.0f, -1.3f, -0.2f, -0.2f, -2.3f);
        figura.DibujarCuadrado2Color(gl, 0.5f, 3.5f, 1.0f, 5.0f, 3.5f, 0.0f, 3.2f, 5.3f, -4.2f, -8.3f);
        figura.DibujarCuadrado2Color(gl, 0.5f, 3.5f, 1.0f, 5.0f, 3.5f, 0.0f, -3.3f, -1.2f, -4.2f, -8.3f);
        //PUERTA
        figura.DibujarCuadrado2Color(gl, -6.5f, 11.0f, 4.0f, 5.7f, -1.4f, 0.3f, -0.5f, 0.7f, -4.2f, -8.9f);
        figura.DibujarCuadrado2Color(gl, -6.5f, 11.0f, 4.0f,5.7f, -1.4f, 0.3f, 1.1f, 2.3f, -4.2f, -8.9f);
        
    //FINALIZADO CASA
    }
    
}
